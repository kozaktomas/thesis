# Možnosti provozu PHP aplikace #

## Úvod a cíl práce ##

### Úvod ###
Dnešní informační společnost je založena na informačních a komunikačních zařízeních, které výrazně proměňují vnímání okolního světa. Poslat zprávu, fotografii nebo video na druhý konec 
světa během několika minisekund již není nepřekonatelný problém a někteří jedinci si život bez těchto technologií nedokáží ani představit.

*Wolrd wide web* je neodmyslitelnou součástí této přeměny společnosti, se kterou je nutné počítat ještě po dlouhá léta. 
Webové rozhraní už neslouží jen jako zdroj informací, ale lze přes něj nakupovat, prohlížet média (fotky, filmy), chatovat nebo přes něj vést sociální život (rozvoj sociálních sítí). 
Počet uživatelů, které využívají tyto služby na internetu dramaticky přibývá a provozovatelé těchto služeb se na tento nárůst musí důkladně připravit. 
Webová služba již není jen staticky uložená webová prezentace, ale rozsáhlé množství systémů, které spolu spolupracují ve snaze poskytnout svým klientům co možná nejlepší
služby (návštěvu, nákup nebo cokoliv jiného).
 
Zřejmě nejzranitelnějším druhem webové aplikace jsou takové aplikace, ve kterých jejich nedostupnout způsobuje finanční ztrátů svému majiteli. 
Typickým příkladem takového druhu aplikace je internetový obchod. Pokud obchod nefunguje, zákazník si nemůže objednat zboží, a tím vzniká poskytovaleli služby ztráta. 
Z tohoto důvodu je důležité se touto problematikou zabývat již v průběhu návrhu systému a ne až v momentně, kdy se to stává kritické pro byznys. 
Systém jako takový by neměl mít žádné závážné bezpečnostní problémy a měl by být dostatečně výkoný k uspokojení všech požadavků. Tím není myšlena pouze strana IS/ICT, ale i systém
z pohledu skladového hospodářství, expedice zboží, zákaznická podpora, atp. Tato diplomová práce se však zaměří pouze na na IS/ICT část a to především na výkoností část systému.

### Cíl práce ###
Cílem práce je zhodnotit možnosti provozu aplikace napsané v jazyce PHP z výkonostního hlediska. Bude navržen a následně implementován e-commerce systém, na kterém budou provedeny 
testy, které si berou za cíl odhalit výhody a nevýhody jednotlivých řešení. Důraz bude kladen na alternativní interprety jazyka PHP a na možnost provozu PHP aplikace v cloudovém prostředí.
Dílčím úkolem bude nalezení vhodných doplňujících systémů, které budou mít pozitivní vliv na výkonost aplikace.


## Možnosti provozu PHP aplikace ##

### Historie PHP a jeho verze ###
<!--- http://php.net/manual/en/history.php.php -->
#### Prvopočátky PHP ####
PHP, které známe dnes, vzniklo v roce 1994 a jeho autorem se stal Rasmus Lerdorf jako tzv. PHP Tool. Postupem času Rasmus přepsal PHP Tool a začal produkovat větší a bohatší implementaci, která byla schopná např. komunikovat z databází a byla vhodná k vytváření jednoduchých dynamických webových aplikací. V roce 1995 Rasmus vydal zdrojové kódy PHP Tool, což dovolilo i ostatním vývojářům té doby používat a hlavně zlepšovat jeho produkt. V září roku 1994 Rasmus vydává kompletně přepsané PHP, ale upustil od jeho jména. V té době ho nazýval jako FI (zkratka pro "Forms Interpreter"), nová implementace zahrnuty některé základní funkce PHP, které přetrvali až do dnešní doby. V říjnu byla vydána další verze, tentokrát už zase pod jménem PHP, což je brána jako vůbec první release PHP. Jazyk byl koncipován tak, aby byl podobný struktuře C a nedělal problémy s přechodem z C, Perlu a podobných jazyků. V roce 1996 vychází  další vydání pod názvem PHP/FI ve kterém přibyla podpora pro databáze (Postgres95), cookies nebo uživatelsky definovaé funkce. Téhož roku pak také vychází PHP 2.0. V dalších pár letech se kód hodně přepisoval a výsledkem bylo PHP 3.0, které se již velice podobalo dnešní verzi. Andi Gutmans and Zeev Suraski z Tel Avivu začli přepisovat parser a také doplňili funkcionalitu, aby bylo možné PHP použít pro eCommerce. Také odstraňují z názvu FI a zůstává jen PHP. V zimě 1998 začalo opět přepisování, které si vzalo za cíle zvýšení výkonu a komplexnosti aplikace. Nový Zend Engine (kompromis ze jmen Zeev a Andi) dostál svých cílů a jeho první představení proběhlo v roce 1999. PHP 4.0 postavené na tomto enginu bylo vydáno o rok později a podporovalo mnoho nových funkcí jako HTTP sessions, outbut buffering, zvýšila se bezpečnost a hlavně do jazyka príbyly nové konstrukce. Po dlouhém vývoji se v roce 2004 dostavá na světlo nová verze 5 s jádrem Zend Engine 2.0 novým objektovým modelem a ostatní novou funkcionalitou.

#### PHP 5 ####
<!--- https://cs.wikipedia.org/wiki/PHP + **PHP doku** -->

Ze začátku vývoje PHP5 se většinou opravovali bugy a doplňovala se pouze okrajová funkcionalita. Důležitější věci se začaly dít od roku 2010.

- 5.0 červenec 2004 - Zend Engine II
- 5.1 listopad 2005 - časové zóny
- 5.2 listopad 2006	- rozšíření json a zip, výkonostní změny
- 5.3 červen 2009 - přelomová verze - jmenné prostory, Closures
- 5.4 březen 2012 - traits, krátká syntaxe pro pole
- 5.5 červen 2013 - operátor yield, blok finally pro ošetřování výjimek, označení extenze MySQL jako zastaralé
- 5.6 srpen 2014 - konstantní skalární výrazy, variadické funkce, upload souborů větších než 2GB

#### PHP 7 ####
<!--- 
https://www.zdrojak.cz/clanky/jake-novinky-prinese-php-7/ 
http://php.net/manual/en/migration70.php
-->
Uvádí se, že díky rozsáhlému refaktorování kódu a kompletní přepracování datových struktur se výrazně zvýšila výkonost PHP. Některé testy uvadí až dvojnásobné zrychlení a 30% úsporu paměti. V této verzi se konečně objevuje typová kontrola skalarních parametrů a typová kontrola návratových hodnot funkcí, bezpečný generátor náhodných čísel, operátor "spaceship", kodování unicode, anonymní třídy a bylo odstraněno vyvolávání chyb programu po zavolaní funkce z nevhodnými parametry (není je vyvolána vyjímka). Opět byly odstraněny funkce, které byly v minulých verzích označeny jako deprecated.


### Provoz PHP aplikace ###

#### Zend engine ###
<!--- https://en.wikipedia.org/wiki/Zend_Engine -->
Zend engine (aktuálně ve verzi 2) byl jádrem PHP již od verze 4. Jedná se o jediný kompletní a zároveň nejrozšířenější implementaci PHP. Zend engine kompiluje PHP kód za chodu do svého vnitřního formátu, který umí spustit. Zend engine zároveň funguje jako referenční implementace, od které jsou ostatní odvozeny. Je kompletně napsán v programovacím jazyku C. Aktuálně se připavuje nová verze 3, které se také označuje jako *phpng*, jež je vyvíjena pro PHP 7.

#### HHVM ###
<!--- https://github.com/facebook/hhvm/wiki/FAQ#general ; http://hhvm.com/blog/2027/faster-and-cheaper-the-evolution-of-the-hhvm-jit ; http://hhvm.com/ ; https://en.wikipedia.org/wiki/HipHop_Virtual_Machine -->
HHVM (Hip Hop Virtual Machine) je virtuální stroj pro spouštění programů napsaných v jazycích hack a PHP napsaný společností Facebook, který HHVM používá pro běh jak ve vývojovém, tak produkčím prostředí. Omezením pro některé uživatele může být fakt, že jidinný skutečne podporovaný operační systém je Linux. Kompletní zdrojové kódy jsou otevřené.  HHVM by mělo být provozováno na serverech společně s FastCGI webserverem jako je např. Apache nebo ngnix.
HHVM používá just-in-time kompilaci a chlubí se především svým výkonem. Na druhou stranu omezuje jazyk PHP o některé možnosti zápisu:

- goto, if:...endif;
- AND, OR, XOR
- reference
- záměna statického volání metody (A::foo() vs. $a->foo() )
- konstrukce break N and continue N
- dynamické spouštění kódu pomocí eval and $object->{$var}
- neumožňuje použít nedefinovanou proměnnou
- klíčové slovo global
- tzv. proměnná proměnných ($$a)
- zápis list(, $b), je nutné použít konstrukci list($_, $b)
- použití stringu jako volání funkce ($func = 'myFunc'; $func(1,2);)
- Rozhraní ArrayAccess
- Case-insensitive volání funkcí
- Inkrementace a dekrementace stringu

Všechny tyto konstrukce "slušný" programátor nepoužívá, ale pokud by byl starší kód migrován na HHVM, tyto restrikce mohou způsobit problémy. Většina moderních PHP frameworků se s těmito změnami vyrovnala a jsou plně kompatibilní.

#### Google App Engine ###
<!--- https://cloud.google.com/appengine/docs/php/ -->
Jako první z cloudových bude představen Google Cloud Platform, který umožňuje běh aplikací napsaných v jazycích Python, Java, PHP a Go. Zakladními vlastnosti, které Google nabízí jsou:

- persistentní uložiště pro data s podporou dotazování, řazení a transakcemi
- automatické škálování výkonu a vytovnávání zátěže
- asynchroní frontování požadavků
- možnost naplánovat úlohu ve stanovené době nebo ji spouštět v pravidelných itervalech
- podpora integrace s dalšími cloudovými službami společnosti Google

Platforma Googlu spouští aplikaci v prostředí interpretu PHP 5.5, kterému přidává pár výhod, ale také nevýhod. Mezi výhody lze zařadit např. automatické loadování tříd, takže programátor nemusí volat funkci include pokud chce třídu použít. Další výhodou je platba za využívání Google Enginu, kde se platí od prostředků, které server(y) potřebují pro svůj chod. Mezi velké nevýhody patří nedostupnost všech rozšíření do PHP. Seznam dostupných rozšíření: apc, bcmath, calendar, Core, ctype, date, dom, ereg, filter, FTP, gd, hash, iconv, json, libxml, mbstring, mcrypt, memcache, memcached, mysql, mysqli, mysqlnd, OAuth, openssl, pcre, PDO, pdo_mysql, Reflection, session, shmop, SimpleXML ,soap ,SPL, standard, tokenizer, xdebug (included with SDK), xml, xmlreader, xmlwriter, Zip, zlib. Mezi další nevýhody patří nedostupnost některých standardních funkcí a odlišné chování k sessions. Největším probémem se však zdá být manipulace se soubory. V App Enginu je lokální souborový systém nedostupný k zápisu z důvodů bezpečnosti a možnosti škálování. App Engine poskytuje k potřebě zápisu tzv. Google Cloud Storages stream wrappery, ke kterým však nelze přístupovat jako k normálním souborů na disku. Není možné soubor otevřít v některých režimech, není možné soubory zamykat a nejsou podporovány některé funkce (ftruncate).

<!--- https://cloud.google.com/appengine/docs/php/googlestorage/ -->

#### Amazon EC2 ####
![Ukázka proxy serveru](images/amazon.png)
Amazon Elastic Compute Cloud (Amazon EC2) je služba, která poskytuje dynamickou vypočetní kapacitu v cloudu. Aritektura této platformy pracuje na principu vyrovnávání zátěže mezi jednotlivými instancemi aplikace. V případě této práci o rozširování instancí pro webové servery. Jedná se o naprosto standardí linuxovou distribuci Ubuntu, ale Amazon umožňuje výběr i z jiných možností (Windows, RedHat, Amazon Linux). Tento server je potřeba nastavit, nainstalovat potřebný software (Apache2 + PHP) a zprovoznit na něm aplikaci. Tato instance se dále prohlasí za referenční a vytvoří se z ní tzv. image (obraz systému). Referenční instance se bude při provozu aplikace replikovat a jeho instance se následně přidávat skupiny serverů obsluhující danou aplikaci (server pool). Počet instancí v poolu se může snižovat nebo zvyšovat a tím reagovat na současnou zátěž celého systému. Je však nutné nastavit pravidla, při kterých k tomu bude docházet. Před celou skupinu instancí aplikace je předřazen load balancer (vyrovnávač zátěže), který je nastaven jako vstupní brána pro uživatele přistupujícího k aplikaci. Konfigurovatelné jsou také instance v poolu - lze jim přidělit systémové parametry (počet jader procesoru, pamět RAM a velikost uložiště). Celý tento popsaný systém lze konfugoravat z webového rozhraní AWS management console nebo automaticky pomocí API. 
<!--- https://aws.amazon.com/ec2/ -->
Velkou výhodou tohoto řešení je možnost instalace jakékoliv dodatečné knihovny nebo softwaru, jelikož se jedná o standardní instalace operačního systému. Cenová politika Amazonu je nastavená tak, že se platí pouze za využité zdroje. Konečná cena se pak liší od druhu aplikace.

#### Cloud Foundry (IBM Bluemix) ####
![Ukázka proxy serveru](images/ibm.png)
Cloud foundry je složba zajišťující platformu (PaaS) s otevřeným zdrojovým kódem (https://github.com/cloudfoundry). Cloud foudry umožňuje kompletní konfiguraci platformy včetně možností škálování systému. Cloud foundry obsahuje projekt BOSH, který podporuje tvorbu releasů, deployment a řízení živbotního cyklu od malých aplikací až pro rozsáhlé škálované aplikace. 
<!--- [https://bosh.io/docs/about.html] -->
Nad tímto systémem postavila společnost IBM svoji instanci Cloud Foundry a provozuje ji jako službu, kterou lze využívat k provozu vlastní aplikace. Na tomto prostředí lze spustit a provozovat takřka libovolnou aplikaci, je ale zapotřebí vytvořit sestavovací konfuguraci a následně v cloudovém prostředí aplikaci sestavit. Kompletní konfigurace není úplně jednoduchá, ale existuje velké množství předpřipravených řešení, které je možné použít. Příjemná je také podpora balíčkovacího systému composer.


#### Microsoft Azure ####

<!--- --------------------- -->
## Návrh e-commerce aplikace ##
Základní myšlenkou aplikace je její reálnost. Pokud bychom pátrali po systému, který navštěvuje velké množství lidí, s nejvetší pravdobodobností bychom došli ke zjištění, že se nejedná 
o jeden monolitický systém, ale na několik subsystémů, které spolu komunikují. Jako příklad lze uvést skladové hospodářství, administraci objednávek, atd. Základní dělení aplikací se 
rozlišuje na back office a front office. Back office aplikace se typická tím, že je zákazníkovi skryta a on o její existenci většinou vůbec není. Do této kategorie se řadí např. 
administrace produktů, kde dochozí k doplňování informací, nastavování cen, přiřazování obrázků a jde vlastně o určitou formu CMS (content management system). Narozdíl od back office 
systémů jsou front office systémy viditelné pro zákazníka. Může se např. jednat o samotný obchod nebo pomocí systém pro sledování zásilky, popř. objednávky.

Aplikace, která má být vyvinuta pro účely této diplomové práce, tedy pro účely testování se má co nejvíce podobat popsanému systému, aby jsme dosáhli co možná nejvěrohodněších výsledků 
při testování. Jako první v řadě bude v řadě vyvinut jednoduchý skladový systém, který bude uchovávat pouze základní informace o produktech jako je název produktu, ktrátký popis, 
zařazení do kategorie a v neposlední řadě taky cenu. Tento systém bude ve zbytku práce označován jako *BRAIN*.


Dalším článkem pomyslného řetězu bude aplikace, která bude BRAIN rozšiřovat. V této práci využiji již hotového řešení v podobě systému OpenCart. Jedná se o e-commerce systém s 
otevřeným zdrojovým kódem, kde bude využito části administrace obchodu. Vešeré údaje o produktech budou brány z BRAINu a budou pravidelně synchronizovány do OpenCartu. Je důležité 
poznamentat, že databáze obou aplikací jsou striktně oddělené a oba systému mohou pracovat samostatně. Do systému OpenCart bude doprogramován submodul, který bude dělat pouze synchronizace mezi mezi BRAINem a OpenCartem.

<popis open cart><co umi><duvody proc byl vybran><existence dalsich moznosti><obrazky a lehka dokumentace jak se s nim zachazi>

### Společná architektura aplikací ###
<!--- odcitovat Nette -->
<!--- MVC by Fowler -->
Aplikace vytvořené pro účel této práce jsou napsaná ve frameworku Nette, který rozděluje aplikaci do modelu MVC.
Podstatou MVC je logické rozdělení do tří vrstev, které spolu komunikují. Vrstva **modelu** obsahuje objetky, které representují nějaké informace o problémové doméně (komunikace s databází, komunikace s API, různé výpočty, atp.).
Vrstva pohledu (view) presentuje výsledky převzaté z modelu do nějakého uživatelského rozhraní. V zásadě by pak neměl být probém celé aplikaci vyměnit tuto vrstvu a např. z webové aplikace udělat aplikaci mobilní. V případě této práce bude využito součásti frameworku Nette a jeho knihovny **Latte**. Úlohou **controlleru** je především zpracovávat uživatelský vstup a manipulovat na jeho základě s modelem a pohledem, který aktualizuje odpovídajícím způsobem. V tomto případě je uživatelské rozhraní složeno právě z vrstev controlleru a view. <!--- citace -->

Nette používá místo controllerů presentery. V zásadě se podobá controlleru z MVC, ale je striktnější ke komunikaci mezi modelem a view. Funguje jako jakási mezivrstva mazi modelem a view. Zpracování požadavků je stejné jako v případě controlleru. V Nette se obvykle pod pojmem presenter myslí potomek třídy Nette\Application\UI\Presenter. Podle příchozích požadavků spouští odpovídající akce a vykresluje šablony. <!--- citace Nette -->

Šablonovací systém Latte, který je součástí Nette frameworku, umožňuje poměrně hezký a přehledný zápis kódu, jehož výstupem má být odpověď ve formátu HTML. Obsahuje nejrůznější marka pro řízení obsahu (if, for, foreach), je možné definovat bloky, které se dají dětit. Dále jsou to filtry na úpravu nebo přeformátování nějakého výstupu. Makra i filtry si lze do Nette doprogramovat a používat tak rozšířenou syntaxi a funkcionalitu. Hlavní výhodou Latte oprati konkurenčním šablonovacím systémům (Twig, Smarty) je bezpečnost. Latte se snaží automaticky vyhodnotit kontext, ve kterém mají být jednotlivé proměnné vykresleny, čímž šetří vývojáří podstatné množství práce a tím i času.

Na modelovou vrstvu má Nette framework balíček Nette database, která je prakticky obálkou nad standardní třídou PDO, která je součástí PHP. Umožňuje efektivnější zápis SQL dotazů a automatické escapování. Nette i tak modelovou stránku moc neřeší a nechává čistě na programátorovi zda využije Nette database, použije některé z frameworků pro objektově relační mapování nebo zvolí jakoukoliv jinou možnost.
 
![Rozdíl mezi MVC a MVP](images/mvc_mvp.png)

### Balíček Ower ###
Bálíček Ower byl vyvinut pro účely této platformy a pomocí **balíčkovacího nastroje composer** importován do všech ostatních aplikací. Ower obsahuje jednoduchou implementaci 2 tříd:

- Environment - třída na detekci prostředí, ve kterém se aplikace nachází
- Api - třída určená ke komunikaci s ostatními aplikacemi v platformě

Celý balíček je pak zavislí na balíčcích nette/utils, aby třídy v Oweru splňovali standardy Nette (reflexe) a nette/caching, díky které si je API schopno zacachovat adresy všech služeb v platformě. Poslední součastí balíku je vyjímka BadApplicationException, která nastavá ve chvíli, kdy se API snaží zavolat neexistující službu. 

![Class model aplikace Ower](images/ower.png)

### Aplikace SCUD ###
Aplikace SCUD slouží ke společnému přihlašování do ostatních aplikací. Vznikla z důvodů, aby si správce aplikace nemusel pamatovat velké množství hesel. Takhle se pouze jednou přihlásí přes SCUD, který ho je schopen přesměrovat na požadovanou aplikaci již v přihlášeném stavu. Pro jednotlivé uživatele je možné nastavit jednotlivá oprávnění, do kterých aplikací bude mít přístup. Na základě požadavku na přesměrování SCUD vygeneruje klíč (access token), se kterým uživatele přesměruje na aplikaci. Aplikace se pak následně ověří ve SCUDu, zda je klíč správný a na jeho základě uživatele přihlásí a vpustí do aplikace.

![Sekvenční schéma aplikace SCUD](navrh/scud_sekvencni.png)

Databáze obsahuje pouze 4 tabulky. V tabulce uživatel (user) jsou uloženy všechny potřebné údaje o uživatelích včetně zahashovaného hesla. Tabulkace se seznamem aplikací (application) uchovává dostupné aplikace, které jsou přes tabulku práv (permissions) provázány s uživatelem. Na základě těchto 3 tabulek se v aplikaci rozhodně, zda má či nemá uživatel dostateční oprávnění, a pokud má, klíč (token) je zapsán do tabulky přístupů (access), podle kterého se následně provede ověření.  

![Databázové schéma aplikace SCUD](navrh/scud_dbs.png)

#### Struktura aplikace ####

**Třídy modelu:**

- Entity aplikace (Application) a přístupový klíč (AccessToken) - jednoduché entity na přepravu dat
- AccessTokenManager - generovaní, mazaní a kontrola klíče
- ApplicationManager - třída pro práci s aplikacemi
- UserManager - třída pro práci s uživateli včetně přihlašování

**Presentery**

- BasePresenter - odič pro ostatní presentery, zajišťuje aby uživatel byl přihlášený a předává do view společné proměnné
- SignPresenter - přihášení do aplikace SCUD
- TransitionPresenter - zvolení koncové aplikace s přesměrováním
- CheckPresenter - rozhraní sloužící ke kontrole klíče (access tokenu) odstatním aplikacím

### Aplikace BRAIN ###
Jak již bylo zmíněno, BRAIN bude simulovat skladový systém, kde budou uchovány základní data o produktech. Konkrétně název produktu, jeho cena, stručný popis, zařazení do kategorie a hlavně jeho dostupnost v jednotlivých skladech. BRAIN nepočítá s žádnými dodatečnými rozšířeními jako je překlad do více jazyků nebo personalizace cen. Opravdu se nebude jednat o nic víc, než o jednoduchý skladovací systém. Brain bude každý den dělat export svých souborů do formátu XML, které následně nahraje na FTP server, kde bude dostupný pro všechny ostatní aplikace, které budeou tyto data vyžadovat.

#### Návrh schématu relační databáze ####
![Databázové schéma aplikace BRAIN](navrh/brain_dbs.png)

Ke každému produktu je uchováván jeho název (title), externí identifikátor (sku), cena (price), záznam o tom, zda je produkt aktivní (active), datum a čas vytvoření a poslední aktualizace (created, updated) a odkazy do tabulek značky (brand) a kateogrie (category). V případě značky je tato vazba nepovinná, teda produkt nemusí podléhat žádné značce. Seznam skladů je uložen v tabulce warehouse, která obsahuje kód skladu (code), jeho název (title), adresu včetně města a poštovního směrovacího čísla (street, city, postcode) a zeměpisné souřadnice (lat, lon). Vazba mezi skladem a produktem je uchována v tabulce product_warehouse, kde existují sloupce s aktuální informací o skladovosti v kusech (total) a datum poslední změny (updated). Pokud vazba mezi produktem a skladem neexistuje, počátá se s tím, že v daném skladu není. Aplikace zajistí, aby se neobjevoval záznam se skladem 0, ale aby byl záznam z tabulky rovnou smazán.

#### Struktura aplikace ####
Aplikace byla napsána od základů pomocí již zmíněné společné architektury.

**Třídy modelu:**

- AbstractManager - rodič pro všechny ostatní managery, zajišťuje připojení do databáze a spouští metoru startup
- BrandManager - manipulace se značkami
- CategoryManager - manipulace s kategoriemi
- WarehouseManager - manipulace se sklady
- Vrstva produkt, která je umístěna ve jmenném prostoru Pogodi\Product a dokáže se plně persistovat do databáze na zákledě současného stavu
- FtpClient - třída dokáže nahrát na FTP server požadovaný soubor
- XmlResponse - rozšiřuje Nette response o XML

**Presentery**

- BasePresenter - rodič pro ostatní presentery, zajišťuje aby uživatel byl přihlášený a předává do view společné proměnné
- BrandPresenter - administrace značek
- CategoryPresenter - administrace kategorií
- DashboardPresenter - hlavní stránka aplikace
- ExporterPresenter - CRON pro vytvoření exportu souborů do XML a následní nahrání na FTP server
- Product presenter - administrace produktů + řízení počtu produktů na skladě
- WarehousePresenter - administrace skladů
- WelcomePresenter - přivítání nepřihlášeného uživatele (presenter nedědí od BasePresenteru)
- ScudPresenter - přihlašování pomocí společné aplikace 

#### Vstupy a výstupy ####
Vstupem do aplikace je v této práci myšleno zadávání jednotlivých produktů do BRAINu ručně uživatelem. Je ovšem možné jeho rozšíření do formy virtuálního skladu, kde by stál jako mezivrstva mezi systémy reálného skladu (např. podnikovým informačním systémem) a administrací elektronického obchodu. Hlavním výstupem aplikace je XML soubor se všemi aktivními produkty, který je nahrán na FTP server pro účely ostatních aplikací. 

### OpenCart ###
Opencart rozšiřuje možnosti BRAINu, od kterého příjmá základní údaje a informace o skladovosti. Narozdíl od skladovacího systému bylo zvolano využítí již hotového řešení. Důvodem 
tohoto rozhodutí byl fakt, že velké množství e-commerce aplikací nějaký podobný systém využívájí a existuje velké množství firem, které administrační prostředí řeší právě využitím 
některého z volně dostupných systémů. Tato aplikace se svoji složitostí řadí už mezi větší. OpenCart se může pochlubit velkým možnostím funkcionality jako:

- Administrace kategorií - které můžou být mezi sebou provázané do stromové struktury

- Tagy - lze vytvořit štítky, které pak mohou být dále asociovány k produktům - Ceny - k produktu jde vždy přiřadit pouze jedna cena + možnost nastavení daně s definovaného seznamu

- Měny - cenu lze přiřadit pouze jednu, ale existuje možnost mít uloženo více měn

- Administrace produktů - všechny popisky produktů, kategorie, atp. mohou být k produktu přiřazeny se zavyslostí na jazyku (v případě této práce se počítá pouze s jedním jazykem)

- Sptáva atributů produktu - lze si předpřipravit různé seznamy atributů, které poté jde přiřadit k jednotlivým produktům podle potřeby

- Správa objednávek - kompletní správa objednávek včetně asociace na objednané produkty

OpenCart obsahuje i další funkcionalitu a možnost uchování více informací, ale pro tuto diplomovou práci bude výše uvedený výčet dostačující.

![Ukázka aplikace OPENCART](images/opencart.png)

### Propojení OPENCARTu a BRAINu ###
Od BRAINu se očekává, že to bude subsystém, který nebude mít závislosti na ostatních subsystémech a bude fungovat zcela somostatně. Komunikaci tedy nebude aktivně vytvářet. Z tohoto důvodu bylo rozhodnuto, že BRAIN bude v pravidelných intervalech generovat XML feed, který si budou moci konzumenti (v tomto případě OpenCart) stáhnout a libovolně zpracovávat. Některé údaje o produktu jsou uchovány v obou systémech, proto je nutné určit priority jednotlivých atributů. Název, stručný popis a zařazení do kategorie bude prioritně bráno ze systému OpenCart, kde údaje z BRAINu budou použity pouze v případě nového produktu a naplnění výchozích údajů. Informace o stavu skladu a ceně však budou prioritně brány z BRAINu a při každém zpracování OpenCartem musí být aktualizovány, aby reflektovali data ve skladu. Předpokládaná granularita synchronizace je stanovena na 1 - 2 x denně.

### SITE ###
Site je subsystém, který se stará o interakci mezi uživatelem a administrací elektronického obchodu. V tomto konkrétním případě se bude jednat jedinný front office systém. Při návrhu se nepočítá s vlastní databází, ale s využitím přímé komunikace s administrací pomocí REST API, kde si budou brát jak informace o kategoriích a produktech, tak i posílat data o nových objednávkách.

### OpenCart API ###
Aplikace OC Api slouží ke komunikaci **mezi administrací OPEN CARTu a aplikací Site**. OC Api nabízí rozhraní služeb, které může Site volat. Opačná inicializace spojení není možná a komunikace probíhá vždy tímto směrem. Aplikace je napsána tak, že služby se dělí do jednotlivých kategorií, které pak odpovídají Presenterům a jednotlivé akce pak metodám presenteru. Pro jednodušší testování a debugování bylo vyvinuto rozhraní **API tester**, které je dostupné v samotném kořenu webu. Tester po vybrání služby nabídne formulář s popisky a datovými typy parametrů pro jednotlivé služby. Výstup se pak pak snaží čitelně zformátovat. Aplikace je přímo připojená do stejné databáze jako OpenCart a provádí nad ní veškeré operace.

![Ukázka API testeru](images/oc_api.png)

#### Nabízené služby ####

- Product
	- getProductById - vrací data o produktu na základě jazyka a unikátního identifikátoru (id)
	- getProductImages - vrací dostupné obrázky pro produkt na základě id
	- getProductAttributes - vrací doplňující atributy k produktu na základě id
	- getProductsInCategory - vrací seznam produktů v kategorii na základě jazyka a id kategorie
	- getProductsInFilter - vrací seznam produktů na základě jazyka a zvoleném filtru (např. hlavní strana)
- Category
	- getAllCategories - vrací všechny dostupné kategorie na základě jazyka
- System
	- info - vrací systémové údaje (např. kde se fyzicky nachází jednotlivé obrázky)
- Fake
	- delay - služba simuluje jakoukoliv úlohu, kterou by mělo API zpracovávat (např. žádost o zařazení do emailových rozesílek). V tomto důsledku není nutné implementovat pro testovací účely celé API, ale je možné využít tuto. V parametru sekundy se mu dá uvést, jak dlouho má operace trvat. Služba automaticky tento interval dodrží

### Arichitektura komunikace v aplikaci SITE ###

V aplikaci SITE vzikla třída **Caller**, která se stará o volaní vše požadavku na API. Co se má volat pak zajišťují jednotlivé sužby implementované v aplikaci. Služby mají společného předka, který jim zajišťuje závislost na prostředí (třída Environment z OWERu), třídě Caller a obecném uložišti pŕo cachování odpovědí (IStorage). V jednotlivých službách (Cateogry, Fake, Product, Order, System) se pak provádí sestavení požadavku a předaní Calleru, popř. operace spojení s cache. Všechny tyto služby jsou pak předány presenterům, které ho využívají pro výpis relevantních informací v elektronickém obchodě. 

## Testování aplikace ##

### Metodika testování ###
Vlastní testování bude rozděleno do dvou základních skupin. První boudou možnosti interpretu, kde si práce bere za cíl odhalit  výkoností rozdíly mezi jednotlivými verzemi PHP a HHVM. Bude vytvořen testovací script pro testování výkonu (rychlost zpracování, využití paměti), který bude spuštěn na serveru se stejnými podmínkami opakovaně pro různé interprety jazyka PHP. Druhá část testování se zaměří na možnosti platformy, tedy na možnost nasadit aplikaci do cloudu. Jelikož se jednotlivé platformy nedají zcela objektivně srovnat, je nutné použít alternativní ukazatele jako složitost nasazení aplikace, možnosti konfigurace prostředí a různé omezení platformy. Na každé prostředí bude spuštěn zátěžový test, kde cílem je odhalení chování platformy pod vysokou zátěží. Na základě těchto parametrů bude navženy možnosti, kdy je danou platformu vhodné využít a upozornit uživatele na potenciální nedostatky.

### Typy testování ###
<!--- 
[1] ISBN: 978-1-118-03196-4 
[2] (http://stackoverflow.com/questions/9750509/load-vs-stress-testing)
-->
Software (včetně webových aplikací) je možné testovat z nejrůznějších pohledů. Zcela automatizovaně je možné testovat funkcionalitu pomocí unit testů nebo integračních testů. Existují testy, které se zaměřují jen na cíl, ale vůbec netestují průběh a další nespočet dalších. Tato diplomová práce se bude zabývat jen výkonostními testy i přes to, že některé služby aplikace jsou plně pokryty integračními testy. Práce se zaměří především na kategorii stress (load) testů, tedy na stav, kdy je aplikace pod velkým tlakem a je nucená zpracovávat abnormálně velké množství požadavků od svých uživatelů. [1] Je také nutné rozlišovat stress test a load test. Load test si bere za úkol zjistit, jaký maximální výkon daná aplikace má. Hlavní podmínkou je relevantní výstup pro uživatele a žadné nestandardní chování aplikace. Stress test ovšem zkoumá jaké je chování aplikace pokud dojde k překročení výkonostních limitů. Práce se bude zajímat výkonem, při kterém je chování standardní a doba odezvy je přijatelná - proto se bude ve zbytku práce hovořit o load testech. [2]

Hned na začátku výběru vhodného softwaru pro load testy byly nastaveny podmínky, které musí aplikace splňovat:
1) škálovatelnost - je nutné, aby bylo možné test spustit z více vláken.
2) multiplatformnost - je možné, že v rámci zvýšení výkonu bude nutné přejít na jiné prostředí a pokud by došlo k výměně operačního systému, nesmí dojít k nekompatibilitě.
3) jednoduché vytvoření scénáře - ideálně v nějakém GUI
4) jednoduché spuštění a zastavení testu - pro případy nouze (např. hroutícího se prostředí)

<!--- http://jmeter.apache.org/ -->
Pomyslným výtězem se stala aplikace jmeter, která se v oblastí load testování stala téměř standardem. Není určená jen pro testování webových aplikací, ale poradí si z celou řadou dalších jako např. FTP, databáze, LDAP, SMTP, TCP, atd. Aplikace je napsaná v programovacím jazyku JAVA a má kompletně otevřené zdrojové kódy. Test jde spustit z více strojů pomocí ne zclea intuitivního GUI. Vytváření testů v jemetru je opravdu jednoduché - program je schopen vytvořit proxy server, který si uživatel poté nastavý do prohlížeče a program zachytává všechny požadavky jdoucí přes něj a kompletuje ho do testu. Tímto způsobem jde "nahrát" chování uživatele a vytvořit tak testovací scénář. Výsledkem je soubor, který obsahuje testovací scénáře a může být spuštěn na jakémkoliv prostředí.


### Metodika vytvoření Load testu a segmentace uživatelů 
<!--- zdroj: http://www.seminarky.cz/Segmentace-uzivatelu-internetu-diplomova-prace-6121 -->

Autor dle své metodiky rozdělil uživatele do 4 základních skupin, které specifikují jejich dosaženou úroveň práce s počítačem, což bylo převzato do prostředí internetu a schopnoti nakupovat v elektornickém obchodě. Tyto skupiny jsou:

1) uživatelé, kteří nemají s internetem ani s počítačem žádnou zkušenost (důchodci, lidé s nízkými příjmy)
2) uživatelé, kteří se k internetu nepřipojují častěji než několikrát za měsíc (nizké příjmy, hledání praktických informací)
3) uživatelé, kteří se k internetu připojují denně anebo alespoň několikrát do týdne (pracující lidé, školáci)
4) uživatelé, kteří na internetu tráví více než 3 hodiny denně (studenti, počítačový experti)

Tyto skupiny mají ve společnosti zastoupení:

| Skupina 	| % zastoupení 						|
| 1.		| 9 %								|
| 2.		| 17 %								|
| 3.		| 63 %								|
| 4.		| 11 %								|

Budou vytvořeny 4 scénáře procházení webu tak, aby bylo zachyceno co možná nejvěryhodnější chování jednotlivých skupin uživatelů. Pro 1. skupinu lidí, kteří neumí s internetem a obecně s počítačem příliš dobře pracovat je plán vytvořit scénář, ve kterém se budou jednotlivé stránky obchodu procházet pomaleji a důraz bude také kladen na možnost vracení se zpět, protože se dá očekám špatná orientace v hierarchii webu. U 2. skupiny bude procházení rychlejší a stránku budou používat jiným způsobem (např. si nechají posílat informace o slevách do emailu). Pro 3. skupinu tvořenou z již zkušenějších uživatelů bude typické "nebát se na něco kliknout" (např. využití košíku ke zboží, které si chci potencioláně koupit) a obecně bude interakce s aplikací rychlejší. 4. skupina se od 3. v zásadě moc neliší, až na pár drobností, které jsou většinou dány vyšší úrovní znalostí práce na počítači (např. si otevřou velké množství nových panelů s produkty, které se jim líbí, a jsou schopni pracovat třeba i s 50 aktivními panely). U scénářů nebude dodrženo pouze chování jednotlivých skupin, ale také procentuální zastoupení ve společnosti.

V případě testování reálné webové aplikace by byly použity analytické údaje, které by byly využity ke tvorbě scénářů. Tyto testy se pak velice přibližují reálnému prostředí a výsledky testu jsou vysoce spolehlivé. [ODKAZ NA DIXONS].

### Tvorba Load testu
Jak již bylo zmíněno, Jmeter umí zachytávat průchod webovou aplikací pomocí proxy serveru. Této funkcionality bylo v tomto případě využito. Před tím, než začne samotné zaznamenávání, je nutné jmeter nastavit. V Jmeteru existuje nepřeberné množství nejrůznějších nastavení, ze kterých bylo využito pouze portu pro proxy server a seznamu vzorů pro adresy, které se mají nebo nemají zaznamenávat a Constant Timeru na zaznamenání časové mezery mezi jednotlivými požadavky na aplikaci.
![Ukázka proxy serveru](images/jmeter_proxy_server.png)
Jmeter pak z jednotlivých požadavků dělá záznamy do zvolené testovací skupiny (target controller). Na následujícím obrázku můžete vidět požadavek při odeslání nákupního košíku. Jedná se o požadavek typu POST, který směřuje na adresu */basket/* a obsahuje parametry zadané uživatelem při vyplnění košíku. Jmeter pak zachovává i HTTP hlavičky, kódovaní znaků i prodlevu mezi jednotlivými požadavky.
![Ukázka proxy serveru](images/jmeter_post_request.png)

### Distrubuované testování
<!--- [http://jmeter.apache.org/usermanual/jmeter_distributed_testing_step_by_step.pdf -->

Jmeter je schopen spouštět testy v distribuovaném prostředí tak, že existuje vždy jeden "master" server, který ovladá ostatní "slave" servery. Na master serveru jsou vždy uloženy informace o load testu a je jedinný, který fyzicky neodesílá žádne HTTP requesty na target (testovaná aplikace). Odesílát požadavky je úkolem slave serverů, které pak odesílají výsledky zpět na master. Podmínkou tohoto prostředí je, aby všechny servery byly dostupné na jedné síti a mohli spolu komunikovat. V případě této práce bylo vytvořeno prostředí tří serverů, které byly umístěny v datacentru, ale požadavkem bylo, aby tester mohl celý průběh testu ovládat ze svého osobního počítače. Z tohoto důvodu byl v datacentru vytvořen ještě jeden server, který bude sloužit jako VPN brána. Tester se pak pomocí VPN připojí přímo do LAN sítě v datacentru a může ovládat všechny slave servery.
![Schéma testovacího prostředí](images/schema_testy.png)

<!--- http://www.pefka.mendelu.cz/files/jaknadip.pdf -->

## Možnosti vylepšení výkonosti ##
Výkonost aplikace lze zvyšovat nejrůznějšími způsoby od vylepšování algoritmického řešení jako takového (časová a prostorová složitost, paralelizace) po využití nejrůznějších nástrojů sloužící pro tyto účely (indexy v databázi, cache). Kolem komunity PHP se točí celá řada nástrojů, které si svoje místo ve světe PHP vydobyli především podporou pro tento jazyk nebo vstřícným chováním jejich autorů. Většina z nich je uvolněna pod volnou licencí a některým z jich bude věnován kousek v následujícím textu o dalších možnostech zlepšení výkonosti.

### Časová a prostorová složitost ###
Aplikací je možné rozložit do velkého množství procedur a funkcí, které vždy řeší danou úlohu. Na každou takovou úlohu pak lze pohlížet jako na samostatný algoritmus, ke kterému lze vždy vypočítat jeho časovou a prostorovou složitost. V ideálním případě by měl programátor při vytváření jednotlivých algoritmů využít nejlepší možná řešení, která by mu zajišťovala co možná nejnižší požadavky na čas a pamět, ale skutečnost bývá taková, že je vždy co vylepšovat. Nemusí se nutně vždy nutně jednat o přepsání celeho algoritmu do třídy s nižší asymptotickou složitostí, ale například o odstranění nepotřebné alokované paměti (nevyužíté proměnné) nebo o odstranění nepotřebného výpočtu. Nedá se však od této možnosti čekat zrychlení ve stovkách procent. V drtivé většině případů se bude jednat o kosmetické úpravy a po výkoností stránce se bude jednat spíše zanedbatelný pokrok. Příkladem, kdy bude možné využít této možnosti optimalizace, je zpracování velkého XML souboru, kdy přepis do podoby využívající knihovny XMLReader, která XML zpracovává jako stream, ze zpracování pomocí DOMDOcument může přinést snížení prostorové složitosti i o stovky procent.

### Optimalizace báze dat ###
- Výběr vhodné databáze (objektová, relační, dokumentová, souborová)
- optimalizace uvnitř databáze - složky, indexy
- rychlost relačních databází - možná nějaký porovnání mysql / postgres / oracle
- zmínit, kdy je co vhodný ... ?

### Zařazení požadavků do fronty ###
<!---
https://www.rabbitmq.com/
-->
Čas zpracování nějaké akce lze zrychlit tak, že některé kroky procesu přeskočíme a tyto vynechané segmenty zařadíme do fronty pro následné zpracování. Typickým příkladem použítí teto metody je odeslání emailu po dokončení objednávkového procesu v elektronickém obchdu. Email nemusí být odeslán neprodlěně při odeslání objednávkového formuláře, ale může několik minut počkat. Stačí si do fronty přidat záznam, že tento email má být odeslán. Výhodou je možnost paralelního zpracování fronty - může existovat více konzumentů. Implementace fronty je možná pomocí tabulky v relační databázi, kde ovšem vziká problém v případě více konzumentů, protože vždy když si bude kontument brát další práci, bude nutné tabulku zamknout pro čtení a zápis. Další možnosti vytvoření fronty je využití specializované služby jako je např. RabbitMQ, který má oficiální knihovnu pro jazyk PHP. Další výhodou tohoto řešení je možnost naprogramovat konzumenta v zcela jiném programovacím jazyce optimalizovaném pro danou úlohu.
![Schéma RabbitMQ pro více konzumentů](images/rabbit.png)

### Optimalizace vyhledávání ###
<!--- 
https://lucene.apache.org/core/ 
https://www.zdrojak.cz/clanky/elasticsearch-vyhledavame-cesky/
https://www.elastic.co/products/elasticsearch
https://github.com/elastic/elasticsearch-php
-->
Vyhledávání jako takové je důležité v každé aplikaci. Otázkou je, do jaké míry je v dané konkrétní aplikaci vyhledávání využito. Pokud se jedná o filtraci do výpisu kategorie, popř. seřazení podle klíčových atributů, bude zřejmě stačit využít relační databázi. Pokud ovšem bude třeba dělat pokročilejší selekci nad daty včetně fulltextového vyhledávání, jako lepší varianta se nabízí využít službu, která slouží přesně pro tyto účely. Lze využít např. Elasticseatch, což je vyhledávácí služba postavená na Apache Lucene. Lucene je fulltextový vyhledávací engine pro textová data, jehož největší výhody jsou vysoká výkonost a možnost škálovatelnosti. S nádstavbou Elasticsearch pak aplikace komunikuje pomocí RESTfull API a data jsou vracena ve formátu JSON. Pro PHP existuje oficiální knihovna pro komunikaci s Elasticsearch. Zřejmě z tohoto důvodu je mezi PHP komunitou tak rozšířený a stal se standardem v oblasti vyhledávání ve webových aplikacích naspaných v jazyce PHP.

### Cachovaní ###
Cachováním se rozumí uložení výsledku nějaké spouštěné úlohy. Pokud je tato úloha vyvolávána opakovaně, je možné přeskočit celý výpočet a rovnou vrátit  výsledek, který je uložený v cache. V aplikaci je nutné odhalid dlouho tvrající bloky, kde by cache mohla pomoci. Nemusí se jednat ani o výpočetně složité úlohy, ale např. i o čtení nějakých dat ze souboru, volání API nebo složitější dotaz do databáze. Cache bývá několikanásobně rychlejší, ale bohužel to s sebou přináší i určité nevýhody, kde největším problémem je neaktuálnost dat. Např. pokud by byla zacachována informace o ceně produktu a někdo ji v administraci elektronického obchodu změnil, k její změně na stránce může dojít až po expiraci cache. Proto je nutné najít vhodnou dobu, po kterou mohou být data uloženy nebo programově najít způsob, jak odstranit takto změněný záznam z cache (nemusí vždy být uplně jednoduché). Cachovat je možné hned na několika vrstvách aplikace - jednotlivé možnosti a technologie budou představeny v následujícím textu.

#### Souborová cache ####
<!--- http://php.net/manual/en/function.serialize.php -->
Všechny moderní PHP frameworky umožňují cachování do souborů. Většinou je tato možnost implementována jako uložení serializovaného objektu (funkce serialize). Toto řešení není v zásadě špatně - je to realativně rychlé a jednoduché na implementaci. Komplikace přicházejí s použitím více aplikačních serverů, kde by si každý musel držet svoji cache nebo by bylo nutné vytvořit napříč všemi servery jeden souborový systém. Tuto možnost lze doporučit menším stránkám, kde se počítá pouze s jedním aplikačním serverem.

#### Memcache ####
<!--- 
odcitovat tech tym - spatna skalovatelnost 
http://php.net/manual/en/book.memcache.php
-->
Memcache je cachovací služba s otevřeným zdrojovým kódem. Jedná se o distribuovaný systém, kde jsou všechny data uloženy v paměti systémém key-value (klíč hodnota). Memcache je používáná do oblastí webů ke cachování výsledků API dotazů, segmentů stránky, atp. Velkou výhodou Memcache je přímá podpora ze strany PHP, kde je možné využít rozšíření (extension) Memcache. V praxi (konkrétně na webech currys.co.uk a pcworld.co.uk) však nastává problém s replikací klíčů po jednotlivých serverech. Memcache od určitého počtu požadavků na zápis dat přestane odpovídat a data ukládat, což může vést až k úplnému zhroucení celé platformy. Proto se toto řešení doporučuje pro středně velké webové aplikace, kde se nepočítá s více instancemi pro cachovací servery.


#### Couchbase ####
<!--- http://www.couchbase.com/nosql-databases/couchbase-server -->
Cauchbase je NoSQL databáze. Jedná se také o distribuovaný systém, který podporuje všechny standardní operace nad daty. Lze také využít jako key-value (klíč-hodnota) uložiště - cache. Pokud se Couchbase využívá tímto způsobem, je velice podobná Memcache. Couchbase ovšem odstraňuje její hlavní nevýhodu, tedy replikaci dat na více serverů. V Couchbase je tato funkcionalita implementována lépe a není problém mít více instancí cache serveru s podporou vyrovnání zátěže. Jedná se ovšem o robustnější řešení a proto se dá doporučit až pro rozsáhlejší systémy.

#### Redis ####
<!--- http://redis.io/ -->
Redis je také open source služba pro ukládání strukturovaných dat, kde je možné ho použít jako samotnou databázi nebo jako cache. Redis nabízí podporu pro standardní typy dat jako jsou řetězce, množiny, pole a navíc podporuje ukládání a operace s prostorovými daty. Redis si v PHP komunitě vysloužil pozici jako vhodný nástroj pro ukládání sessions, k čemuž se náramě hodí.

#### Varnish ####
<!--- Varnish book -->
Varnish je reverzní HTTP proxy server, který se někdy označuje také jako HTTP akcelerátor. Reverzní proxy je server, který se klientům zobrazuje jako primární server. Cílem je, aby již na tomto proxy serveru byla připravená (nacachovaná) odpoveď a požadavek nebyl propuštěn na aplikační servery. Samozřejmostí je možnost nastavit politiku cachování, protože mohou existovat stránky, které není žádoucí mít na Varnish serveru (např. proces nákupního košíku). U e-commerce aplikací je možné Varnish využít na drtivou většinu stránek, kde míra zachycení požadavku na reverzní proxy může dosahovat i 90 % všech požadavků, které směřují na aplikaci (společnost Dixons Carphone). Je jasné, že tato technologie má na výkon aplikace obrovský vliv, ale je důležité zmínit, že možnost využití reverzní proxy je pro každou aplikaci indivituální a není možné udělat nějaké generalizové prohlášení. Varnish má i další možné využití jako je firewall pro webovou aplikaci, obrana proti DDoS útokům, vyrovnání zátěže (load balancer), atd.
![Schéma komunikace s revezní proxy](images/varnish.png)
